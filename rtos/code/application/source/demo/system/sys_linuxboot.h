#ifndef SYS_LINUXBOOT_H
#define SYS_LINUXBOOT_H

typedef struct _FDT_INFO {
	unsigned int linux_addr;
	unsigned int linux_size;
	unsigned int linuxtmp_addr;
	unsigned int linuxtmp_size;
	unsigned int bridge_addr;
	unsigned int bridge_size;
	// following are for check memory partition
	unsigned int dram_addr;
	unsigned int dram_size;
	unsigned int fdt_addr;
	unsigned int fdt_size;
	unsigned int rtos_addr;
	unsigned int rtos_size;
	unsigned int hdal_addr;
	unsigned int hdal_size;
	unsigned int shmem_addr;
	unsigned int shmem_size;
} FDT_INFO;

typedef struct _LINUXTMP_PARTITION {
	unsigned int tmp_begin;
	unsigned int tmp_curr;
	unsigned int tmp_end;
	unsigned int tmp2_begin; // if it available, means small dram.
	unsigned int tmp2_curr;  // use the memory 8MB - rtos_addr
	unsigned int tmp2_end;   // to store fdt and ramdisk
	unsigned int fdt_addr;
	unsigned int fdt_size;
	unsigned int ramfs_addr;
	unsigned int ramfs_size;
	unsigned int bootargs_addr;
	unsigned int bootargs_size;
	unsigned int lz_linux_addr;
	unsigned int lz_linux_size;
	unsigned int nvtpack_addr;
	unsigned int nvtpack_size;
} LINUXTMP_PARTITION;

typedef struct _LINUXBOOT_INFO {
	FDT_INFO fdt_info;
	LINUXTMP_PARTITION linuxtmp;
} LINUXBOOT_INFO;

int linuxboot_setup(LINUXBOOT_INFO *p_info);
void linuxboot_go(LINUXBOOT_INFO *p_linuxtmp);
int linuxboot_set_flash_preload(LINUXBOOT_INFO *p_info); //optional, for fastboot-ai on nor

#endif