#ifndef _ARM_GIC_H
#define _ARM_GIC_H

extern void arm_gic_init(void);
extern int arm_gic_raise_sgi(int it, unsigned char cpu_mask);

#endif

