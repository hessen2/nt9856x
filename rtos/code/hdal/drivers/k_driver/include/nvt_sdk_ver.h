/**
 * @file nvt_sdk_ver.h
 * @brief type definition of SDK release version
 * @date 20220825
 * Copyright Novatek Microelectronics Corp. 2022.  All rights reserved.
 */

#define NVT_SDK_VER 0x101007  // "/proc/SDK/version" should be 1.01.007