/**
	@brief Vendor md implementation version.

	@file kdrv_md_version.h

	@ingroup kdrv_md

	@note Nothing.

	Copyright Novatek Microelectronics Corp. 2019.  All rights reserved.
*/
#ifndef _KDRV_MD_VERSION_H_
#define _KDRV_MD_VERSION_H_


#define KDRV_MD_IMPL_VERSION      "01.01.2206060" //implementation version

#endif
