/********************************************************************
	INCLUDE FILES
********************************************************************/
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include "hdal.h"
#include "hd_type.h"
#include "hd_common.h"
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>
#include "hd_gfx.h"
#include "vendor_ai.h"
#include "kflow_ai_net/kflow_ai_net.h"
#include "vendor_ai_net/nn_net.h"
#include <arm_neon.h>

#define SCALE_BUF_SIZE_      (MAX_FRAME_WIDTH * MAX_FRAME_HEIGHT * 3)
#define PVDAI_VDO_W            416 //416 //1024
#define PVDAI_VDO_H            416 //416 //576

#define PRUNE37PVD           ENABLE
#define LIMIT_FDET_PVD       ENABLE   //ENABLE DISABLE

#define NN_PDCNN_DRAW           ENABLE
#if 0
#define PD_MAX_DISTANCE_MODE    2
#endif

#define PD_MAX_MEM_SIZE 	    (15 * 1024 * 1024)
#define PD_SCALE_BUF_SIZE       (2 * 1024 * 576)

#define PVD_YUV_WIDTH			416 //416 //960
#define PVD_YUV_HEIGHT			416 //416 //540

#define PVD_VERSION_A  "2"
#define PVD_VERSION_B  "2"
#define PVD_VERSION_C  "3"

#define PVDROI_MODE1_WIDTH          	  (FLOAT)0.6640625
#define PVDROI_MODE1_HEIGHT  			  (FLOAT)0.6649306
#define PVDROI_MODE2_WIDTH          	  (FLOAT)0.625
#define PVDROI_MODE2_HEIGHT  			  (FLOAT)0.625
#define PVDROI_MODE3_WIDTH 				  (FLOAT)0.5859375
#define PVDROI_MODE3_HEIGHT				  (FLOAT)0.5833333

#undef MIN
#define MIN(a, b)           ((a) < (b) ? (a) : (b))
#undef MAX
#define MAX(a, b)           ((a) > (b) ? (a) : (b))
#undef CLAMP
#define CLAMP(x,min,max)    (((x) > (max)) ? (max) : (((x) > (min)) ? (x) : (min)))

typedef struct _PVD_PROPOSAL_PARAM {
	UINT32 run_id;
} PVD_PROPOSAL_PARAM;

typedef struct _PVDCNN_MEM {
    INT32 out_num;
	VENDOR_AIS_FLOW_MEM_PARM io_mem;
	VENDOR_AIS_FLOW_MEM_PARM model_mem;
	VENDOR_AIS_FLOW_MEM_PARM scale_buf;
	VENDOR_AIS_FLOW_MEM_PARM input_mem;
	VENDOR_AIS_FLOW_MEM_PARM pps_result;
    VENDOR_AIS_FLOW_MEM_PARM out_result;
	VENDOR_AIS_FLOW_MEM_PARM final_result;
	VENDOR_AIS_FLOW_MEM_PARM backbone_output;
	VENDOR_AIS_FLOW_MEM_PARM reshape_cls;
} PVDCNN_MEM;
typedef struct _PVD_PREDICT {
	float x;
	float y;
	float w;
	float h;
	float confidence;
	int classType;
} PVD_PREDICT;

typedef struct _PVD_RESULT {
	float xmin;
	float ymin;
	float xmax;
	float ymax;
	float score;
	int classtype;
} PVD_RESULT;

typedef struct _SOFTNMS_RESULT
{
	FLOAT 	score;
	FLOAT 	x1;
	FLOAT 	y1;
	FLOAT 	x2;
	FLOAT 	y2;
	FLOAT   areas;
	FLOAT   weight;
	FLOAT   confidence;
	int classType;
}SOFTNMS_RESULT;

typedef struct _PVDLIMIT_PARAM {
	FLOAT score_thrs[25];
	INT32 box_sizes[25];
	INT32 limit_size;
	INT32 sm_thr_num;
	FLOAT k[25];
	INT32 limit_module;
} PVDLIMIT_PARAM;

typedef struct _PVD_LAYER_PARAM {
	INT32 class_num;
	FLOAT nms_threshold;
	FLOAT confindence_threshold;
	INT32 bottom_num;
	INT32 mask_size;
	INT32 anchors_scale[2];
	INT32 size_swap_data;
	INT32 size_class_score;
	INT32 size_predicts_init;
	INT32 size_final_pred;
	INT32 mask[6];
	INT32 biases[12];
	FLOAT cls_score_thresh;
	INT32 mask_group_num;
	INT32 input_size[2];
	PVDLIMIT_PARAM pd_limit_param;
	PVDLIMIT_PARAM vd_limit_param;
} PVD_LAYER_PARAM;

typedef struct _PVDCNN_RESULT
{
	INT32   category;
	FLOAT 	score;
	FLOAT 	x1;
	FLOAT 	y1;
	FLOAT 	x2;
	FLOAT 	y2;
}PVDCNN_RESULT;

typedef struct _PVD_LAYER_MEM {
	PVD_PREDICT* predicts_init;
	PVD_PREDICT* predicts_initVD;
	FLOAT* class_score;
	FLOAT* swap_data;
	PVDCNN_RESULT* final_pred;
} PVD_LAYER_MEM;

typedef struct _PVDAI_RSLT_PARM {
	VENDOR_AIS_FLOW_MEM_PARM rslt_mem;
	UINT32 obj_num;
} PVDAI_RSLT_PARM;

typedef struct _PVD_IRECT {
	FLOAT  x1;                           ///< x coordinate of the top-left point of the rectangle
	FLOAT  y1;                           ///< y coordinate of the top-left point of the rectangle
	FLOAT  x2;                           ///< rectangle width
	FLOAT  y2;                           ///< rectangle height
} PVD_IRECT;

INT32 pvdcnn_detection_out(VENDOR_AI_BUF* layer_buf, PVD_LAYER_PARAM* PVD_param, FLOAT** layer_float, PVD_LAYER_MEM* PVD_det_mem, 
                           UINT32 obj_num, INT32 PDCNN_MODE, INT32 VDCNN_MODE);
INT32 pvdcnn_detection_sample(VENDOR_AI_BUF* layer_buf, PVD_LAYER_PARAM* PVD_param, FLOAT** layer_float, PVD_LAYER_MEM* PVD_det_mem, 
                           UINT32 obj_num, INT32 PDCNN_MODE, INT32 VDCNN_MODE, UINT32* posttime);
INT32 pvdcnn_detectionout_armneon(VENDOR_AI_BUF* layer_buf, PVD_LAYER_PARAM* PVD_param, FLOAT** layer_float, PVD_LAYER_MEM* PVD_det_mem, 
                           UINT32 obj_num, INT32 PDCNN_MODE, INT32 VDCNN_MODE);
INT32 pvdcnn_detectionsample_armneon(VENDOR_AI_BUF* layer_buf, PVD_LAYER_PARAM* PVD_param, FLOAT** layer_float, PVD_LAYER_MEM* PVD_det_mem, 
                           UINT32 obj_num, INT32 PDCNN_MODE, INT32 VDCNN_MODE, UINT32* posttime, UINT32* nmspost);
VOID pvdcnn_getparam(PVD_LAYER_PARAM* PVD_params, INT32* max_map, char* paramsfile);
VOID pdcnn_para_init(PVD_LAYER_PARAM *pvd_param, INT8 pvd_light_night);
VOID vdcnn_para_init(PVD_LAYER_PARAM *pvd_param, INT8 pvd_light_night);
VOID get_slopes(PVDLIMIT_PARAM *limit_para, FLOAT thr);
INT32 dynamic_limit(INT32 src_obj_num, PVDLIMIT_PARAM* limit_para, FLOAT clsscore_thresh, UINT8 pvd_cls, 
                    PVDCNN_RESULT *persons, int typ, UINT32 width, UINT32 height);
VOID gfx_img_to_vendor(VENDOR_AI_BUF* out_img, HD_GFX_IMG_BUF* in_img, UINT32 va);
VOID pvdcnn_get_version(VOID);
HD_RESULT pvdcnn_version_check(VENDOR_AIS_FLOW_MEM_PARM *buf);
VOID pvd_gfx_img_to_vendor(VENDOR_AI_BUF* out_img, HD_GFX_IMG_BUF* in_img, UINT32 va);
HD_RESULT pvdcnn_netprocess(PVD_PROPOSAL_PARAM* proposal_params, PVDCNN_MEM* pdcnn_mem, VENDOR_AI_BUF* src_img, VENDOR_AI_BUF *layer_buffer, 
                                  PVD_LAYER_PARAM* PVD_params, FLOAT **out_layer_float, PVD_LAYER_MEM* PVD_det_mem, PVDCNN_RESULT* objs_info, 
								  UINT32 img_num, UINT32 outlayer_num, 
								  UINT32 max_distance_mode, INT32 PDCNN_MODE, INT32 VDCNN_MODE);
HD_RESULT pvdcnn_crop_img(HD_GFX_IMG_BUF *dst_img, VENDOR_AI_BUF *src_img, HD_GFX_SCALE_QUALITY method, PVD_IRECT *roi);
HD_RESULT get_pvd_mem(VENDOR_AIS_FLOW_MEM_PARM *buf, VENDOR_AIS_FLOW_MEM_PARM *req_mem, UINT32 req_size, UINT32 align_size);
UINT32 pvdcnn_nonmax_suppress(PVDCNN_RESULT *p_boxes, INT32 num, FLOAT ratio, INT32 method, INT32 after_num);
