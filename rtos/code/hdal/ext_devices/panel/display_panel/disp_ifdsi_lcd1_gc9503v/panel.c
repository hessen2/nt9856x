/*
    Display object for driving DSI device

    @file       NT35521.c
    @ingroup
    @note       This panel MUST select ide clock to PLL1 ( 480 ). Once change to \n
				another frequence, the _IDE_FDCLK should be re calculated

    Copyright   Novatek Microelectronics Corp. 2011.  All rights reserved.
*/
#include "dispdev_ifdsi.h"

#define PANEL_WIDTH     320
#define PANEL_HEIGHT    820

#define NT35521_IND_MSG(...)       debug_msg(__VA_ARGS__)

#define NT35521_ERR_MSG(...)       debug_msg(__VA_ARGS__)

#define NT35521_WRN_MSG(...)       debug_msg(__VA_ARGS__)
#define NT35521_TE_OFF              0
#define NT35521_TE_ON               1

#define NT35521_TE_PACKET           0
#define NT35521_TE_PIN              1


#define DSI_FORMAT_RGB565          0    //ide use 480 & DSI use 480
#define DSI_FORMAT_RGB666P         1    //ide use 480 & DSI use 480
#define DSI_FORMAT_RGB666L         2    //ide use 480 & DSI use 480
#define DSI_FORMAT_RGB888          3    //ide use 480 & DSI use 480

#define DSI_OP_MODE_CMD_MODE       1
#define DSI_OP_MODE_VDO_MODE       0
#define DSI_PACKET_FORMAT          DSI_FORMAT_RGB565  //NT35521 only support RGB888

#define DSI_OP_MODE                DSI_OP_MODE_VDO_MODE//DSI_OP_MODE_CMD_MODE

#define DSI_TARGET_CLK             240//160
#define _IDE_FDCLK      60000000//37125000 //(960/24)*4 = 160MHz. (IDE = 297MHz, 297/1= 297MHz)

#define HVALIDST    30 //HBP
#define VVALIDST    20 //VBP
#define HSYNCT      10 //HS
#define VSYNCT      8  //VS

/*
    panel Parameters for TCON NT35521
*/
//@{
/*Used in DSI*/
const T_PANEL_CMD t_cmd_mode_dsi[] = {
    {DSICMD_CMD,0xF0},
    {DSICMD_DATA,0x55},
    {DSICMD_DATA,0xaa},
    {DSICMD_DATA,0x52},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x00},

    {DSICMD_CMD,0xF6},
    {DSICMD_DATA,0x5A},
    {DSICMD_DATA,0x87},

    {DSICMD_CMD,0xcd},
    {DSICMD_DATA,0x25},

    {DSICMD_CMD,0xc9},
    {DSICMD_DATA,0x10},

    {DSICMD_CMD,0xf8},
    {DSICMD_DATA,0x8a},

    {DSICMD_CMD,0xac},
    {DSICMD_DATA,0x65},

    {DSICMD_CMD,0xa7},
    {DSICMD_DATA,0x47},

    {DSICMD_CMD,0xa0},
    {DSICMD_DATA,0xdd},

    {DSICMD_CMD,0x87},
    {DSICMD_DATA,0x04},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x66},

    {DSICMD_CMD,0x86},
    {DSICMD_DATA,0x99},
    {DSICMD_DATA,0xa3},
    {DSICMD_DATA,0xa3},
    {DSICMD_DATA,0x31},

    {DSICMD_CMD,0xfa},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x04},

    {DSICMD_CMD,0xA3},
    {DSICMD_DATA,0x6E},

    {DSICMD_CMD,0xFD},
    {DSICMD_DATA,0x28},
    {DSICMD_DATA,0x3C},
    {DSICMD_DATA,0x00},

    {DSICMD_CMD,0x9a},
    {DSICMD_DATA,0x87},

    {DSICMD_CMD,0x9b},
    {DSICMD_DATA,0x5f},

    {DSICMD_CMD,0x82},
    {DSICMD_DATA,0x3d},
    {DSICMD_DATA,0x3d},

    {DSICMD_CMD,0xb1},
    {DSICMD_DATA,0x10},//rotate 0x10

    {DSICMD_CMD,0x6d},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1E},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x09},
    {DSICMD_DATA,0x0b},
    {DSICMD_DATA,0x0d},
    {DSICMD_DATA,0x0f},
    {DSICMD_DATA,0x07},
    {DSICMD_DATA,0x1E},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1E},
    {DSICMD_DATA,0x1E},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1E},
    {DSICMD_DATA,0x1E},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1E},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x10},
    {DSICMD_DATA,0x0e},
    {DSICMD_DATA,0x0c},
    {DSICMD_DATA,0x0a},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x1E},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1e},
    {DSICMD_DATA,0x1e},

    {DSICMD_CMD,0x64},
    {DSICMD_DATA,0x28},
    {DSICMD_DATA,0x06},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x39},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x28},
    {DSICMD_DATA,0x05},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x3a},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},

    {DSICMD_CMD,0x65},
    {DSICMD_DATA,0x28},
    {DSICMD_DATA,0x04},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x3b},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x28},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x3c},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},

    {DSICMD_CMD,0x66},
    {DSICMD_DATA,0x28},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x3d},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x28},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x3e},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},

    {DSICMD_CMD,0x67},
    {DSICMD_DATA,0x28},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x3f},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x20},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x40},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},

    {DSICMD_CMD,0x60},
    {DSICMD_DATA,0x58},
    {DSICMD_DATA,0x09},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},
    {DSICMD_DATA,0x58},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},

    {DSICMD_CMD,0x69},
    {DSICMD_DATA,0x14},
    {DSICMD_DATA,0x22},
    {DSICMD_DATA,0x14},
    {DSICMD_DATA,0x22},
    {DSICMD_DATA,0x14},
    {DSICMD_DATA,0x22},
    {DSICMD_DATA,0x04},

    {DSICMD_CMD,0x63},
    {DSICMD_DATA,0x53},
    {DSICMD_DATA,0x2f},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},
    {DSICMD_DATA,0x53},
    {DSICMD_DATA,0x30},
    {DSICMD_DATA,0x72},
    {DSICMD_DATA,0x6c},

    {DSICMD_CMD,0x6b},
    {DSICMD_DATA,0x07},

    {DSICMD_CMD,0x7A},
    {DSICMD_DATA,0x0f},
    {DSICMD_DATA,0x13},

    {DSICMD_CMD,0x7B},
    {DSICMD_DATA,0x0f},
    {DSICMD_DATA,0x13},

    {DSICMD_CMD,0xD1},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x2c},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x46},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x67},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x94},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xb8},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x22},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x76},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0xB6},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x18},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x69},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x6a},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0xb5},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x0a},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x43},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x96},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xb4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xd9},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xe8},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xeB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xef},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFF},

    {DSICMD_CMD,0xD2},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x2c},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x46},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x67},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x94},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xb8},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x22},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x76},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0xB6},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x18},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x69},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x6a},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0xb5},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x0a},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x43},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x96},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xb4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xd9},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xe8},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xeB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xef},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFF},

    {DSICMD_CMD,0xD3},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x2c},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x46},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x67},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x94},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xb8},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x22},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x76},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0xB6},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x18},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x69},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x6a},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0xb5},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x0a},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x43},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x96},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xb4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xd9},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xe8},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xeB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xef},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFF},

    {DSICMD_CMD,0xD4},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x2c},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x46},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x67},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x94},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xb8},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x22},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x76},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0xB6},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x18},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x69},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x6a},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0xb5},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x0a},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x43},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x96},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xb4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xd9},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xe8},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xeB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xef},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFF},

    {DSICMD_CMD,0xD5},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x2c},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x46},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x67},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x94},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xb8},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x22},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x76},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0xB6},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x18},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x69},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x6a},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0xb5},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x0a},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x43},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x96},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xb4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xd9},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xe8},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xeB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xef},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFF},

    {DSICMD_CMD,0xD6},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x08},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x2c},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x46},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x67},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0x94},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xb8},
    {DSICMD_DATA,0x00},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x22},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0x76},
    {DSICMD_DATA,0x01},
    {DSICMD_DATA,0xB6},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x18},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x69},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0x6a},
    {DSICMD_DATA,0x02},
    {DSICMD_DATA,0xb5},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x0a},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x43},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0x96},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xb4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xd9},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xe8},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xeB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xef},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xF4},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFB},
    {DSICMD_DATA,0x03},
    {DSICMD_DATA,0xFF},

    //{DSICMD_CMD,0x3a},
    //{DSICMD_DATA,0x55},

    //{DSICMD_CMD,0xb1},
    //{DSICMD_DATA,0x00},
    //{DSICMD_DATA,0x0e},
    //{DSICMD_DATA,0x0e},
    //{DSICMD_DATA,0x14},
    //{DSICMD_DATA,0x04},

    {DSICMD_CMD,0x11},
    {DSICMD_DATA,0x00},

    {CMDDELAY_MS, 100},

    {DSICMD_CMD,0x29},
    {DSICMD_DATA,0x00},

};


const T_PANEL_CMD t_cmd_standby_dsi[] = {
	//{DSICMD_CMD,     0x28},         // Display OFF
	//{CMDDELAY_MS,    10},
	//{DSICMD_CMD,     0x10},      // Sleep in
	//{CMDDELAY_MS,    10},
};

const T_LCD_PARAM t_mode_dsi[] = {
	/***********       MI Serial Format 1      *************/
	{
		// tPANEL_PARAM
		{
			/* Old prototype */
			//PINMUX_DSI_4_LANE_CMD_MODE_RGB565,    //!< LCDMode
			//PINMUX_DSI_4_LANE_VDO_SYNC_EVENT_RGB565,//!< LCDMode
			//PINMUX_DSI_4_LANE_CMD_MODE_RGB565,
			//PINMUX_DSI_4_LANE_CMD_MODE_RGB666L,
#if (DSI_OP_MODE == DSI_OP_MODE_CMD_MODE)
    #if(DSI_PACKET_FORMAT == DSI_FORMAT_RGB888)
			PINMUX_DSI_4_LANE_CMD_MODE_RGB888,
    #elif(DSI_PACKET_FORMAT == DSI_FORMAT_RGB666L)
			PINMUX_DSI_4_LANE_CMD_MODE_RGB666L,
    #elif(DSI_PACKET_FORMAT == DSI_FORMAT_RGB565)
			PINMUX_DSI_1_LANE_CMD_MODE_RGB565,
    #endif
#else
    #if(DSI_PACKET_FORMAT == DSI_FORMAT_RGB888)
			//PINMUX_DSI_4_LANE_VDO_SYNC_PULSE_RGB888,
			PINMUX_DSI_4_LANE_VDO_SYNC_EVENT_RGB888,
    #elif(DSI_PACKET_FORMAT == DSI_FORMAT_RGB666L)
			PINMUX_DSI_4_LANE_VDO_SYNC_PULSE_RGB666L,
			//PINMUX_DSI_4_LANE_VDO_SYNC_EVENT_RGB666L,
    #elif(DSI_PACKET_FORMAT == DSI_FORMAT_RGB666P)
			PINMUX_DSI_4_LANE_VDO_SYNC_PULSE_RGB666P,
			//PINMUX_DSI_4_LANE_VDO_SYNC_EVENT_RGB666P,
    #elif(DSI_PACKET_FORMAT == DSI_FORMAT_RGB565)
			///PINMUX_DSI_4_LANE_VDO_SYNC_PULSE_RGB565,
			PINMUX_DSI_1_LANE_VDO_SYNC_EVENT_RGB565,
    #endif
#endif
			_IDE_FDCLK,                                 //!< fd_clk
            390,                                        //!< uiHSyncTotalPeriod(HTOTAL)
            PANEL_WIDTH,                                //!< ui_hsync_active_period(HACT)
            HVALIDST,                                   //!< uiHSyncBackPorch(HBP)
            868,                                       //!< uiVSyncTotalPeriod(VTOTAL)
            PANEL_HEIGHT,                               //!< uiVSyncActivePeriod
            VVALIDST,                                   //!< uiVSyncBackPorchOdd
            VVALIDST,                                   //!< uiVSyncBackPorchEven
			PANEL_WIDTH,                            //!< ui_buffer_width
			//PANEL_HEIGHT,                           //!< ui_buffer_height
      832,                           //!< ui_buffer_height
			PANEL_WIDTH,                            //!< ui_window_width
			//PANEL_HEIGHT,                           //!< ui_window_height
      832,                           //!< ui_window_height
			FALSE,                                  //!< b_ycbcr_format
			/* New added parameters */
            HSYNCT,                                     //!< uiHSyncSYNCwidth
            VSYNCT                                      //!< uiVSyncSYNCwidth

		},

		// T_IDE_PARAM
		{
			/* Old prototype */
			PINMUX_LCD_SEL_GPIO,            //!< pinmux_select_lcd;
			ICST_CCIR601,                   //!< icst;
			{FALSE, FALSE},                  //!< dithering[2];
			DISPLAY_DEVICE_MIPIDSI,         //!< **DONT-CARE**
			IDE_PDIR_RGB,                   //!< pdir;
			IDE_LCD_R,                      //!< odd;
			IDE_LCD_G,                      //!< even;
			TRUE,                           //!< hsinv;
			TRUE,                           //!< vsinv;
			FALSE,                          //!< hvldinv;
			FALSE,                          //!< vvldinv;
			TRUE,                           //!< clkinv;
			FALSE,                          //!< fieldinv;
			FALSE,                          //!< **DONT-CARE**
			FALSE,                          //!< interlace;
			FALSE,                          //!< **DONT-CARE**
			0x40,                           //!< ctrst;
			0x00,                           //!< brt;
			0x40,                           //!< cmults;
			FALSE,                          //!< cex;
			FALSE,                          //!< **DONT-CARE**
			TRUE,                           //!< **DONT-CARE**
			TRUE,                           //!< tv_powerdown;
			{0x00, 0x00},                   //!< **DONT-CARE**

			/* New added parameters */
			FALSE,                          //!< yc_ex
			FALSE,                          //!< hlpf
			{FALSE, FALSE, FALSE},          //!< subpix_odd[3]
			{FALSE, FALSE, FALSE},          //!< subpix_even[3]
			{IDE_DITHER_5BITS, IDE_DITHER_6BITS, IDE_DITHER_5BITS}, //!< dither_bits[3]
			FALSE                           //!< clk1/2
		},

		(T_PANEL_CMD *)t_cmd_mode_dsi,                 //!< p_cmd_queue
		sizeof(t_cmd_mode_dsi) / sizeof(T_PANEL_CMD),  //!< n_cmd
	}
};

const T_LCD_ROT *t_rot_dsi = NULL;

//@}

T_LCD_ROT *dispdev_get_lcd_rotate_dsi_cmd(UINT32 *mode_number)
{
#if 0
	if (t_rot_dsi != NULL) {
		*mode_number = sizeof(t_rot_dsi) / sizeof(T_LCD_ROT);
	} else
#endif
	{
		*mode_number = 0;
	}
	return (T_LCD_ROT *)t_rot_dsi;
}

T_LCD_PARAM *dispdev_get_config_mode_dsi(UINT32 *mode_number)
{
	*mode_number = sizeof(t_mode_dsi) / sizeof(T_LCD_PARAM);
	return (T_LCD_PARAM *)t_mode_dsi;
}

T_PANEL_CMD *dispdev_get_standby_cmd_dsi(UINT32 *cmd_number)
{
	*cmd_number = sizeof(t_cmd_standby_dsi) / sizeof(T_PANEL_CMD);
	return (T_PANEL_CMD *)t_cmd_standby_dsi;
}


void dispdev_set_dsi_config(DSI_CONFIG *p_dsi_config)
{
#if 0
	// DSI input source clock = 480
	// Target can be 480 / 240 / 160 / 120
	FLOAT   dsi_target_clk = DSI_TARGET_CLK;
	UINT32  div;


	div = (UINT32)(p_dsi_config->f_dsi_src_clk / dsi_target_clk);

	if (div == 0) {
		NT35521_WRN_MSG("div = 0 force ++\r\n");
		div++;
	}
	pll_setClockRate(PLL_CLKSEL_DSI_CLKDIV, PLL_DSI_CLKDIV(div - 1));
#else
	dsi_set_config(DSI_CONFIG_ID_FREQ, DSI_TARGET_CLK * 1000000);
#endif
#if (DSI_TARGET_CLK == 160) //real is 150MHz
	dsi_set_config(DSI_CONFIG_ID_TLPX, 1);
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_GO, 0); //FPGA: 120MHz, GO = 0
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_SURE, 0); //FPGA: 120MHz, SURE = 0

	dsi_set_config(DSI_CONFIG_ID_THS_PREPARE, 1);
	dsi_set_config(DSI_CONFIG_ID_THS_ZERO, 4);
	dsi_set_config(DSI_CONFIG_ID_THS_TRAIL, 2);
	dsi_set_config(DSI_CONFIG_ID_THS_EXIT, 3);

	dsi_set_config(DSI_CONFIG_ID_TCLK_PREPARE, 1);
	dsi_set_config(DSI_CONFIG_ID_TCLK_ZERO, 7);
	dsi_set_config(DSI_CONFIG_ID_TCLK_POST, 8);
	dsi_set_config(DSI_CONFIG_ID_TCLK_PRE, 1);
	dsi_set_config(DSI_CONFIG_ID_TCLK_TRAIL, 1);
#elif(DSI_TARGET_CLK == 960)
	dsi_set_config(DSI_CONFIG_ID_TLPX, 3);
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_GO, 21);
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_SURE, 0);
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_GET, 20);

	dsi_set_config(DSI_CONFIG_ID_THS_PREPARE, 5);//4
	dsi_set_config(DSI_CONFIG_ID_THS_ZERO, 6);
	dsi_set_config(DSI_CONFIG_ID_THS_TRAIL, 3); //7
	dsi_set_config(DSI_CONFIG_ID_THS_EXIT, 6);

	dsi_set_config(DSI_CONFIG_ID_TCLK_PREPARE, 5);//3
	dsi_set_config(DSI_CONFIG_ID_TCLK_ZERO, 16);
	dsi_set_config(DSI_CONFIG_ID_TCLK_POST, 16);
	dsi_set_config(DSI_CONFIG_ID_TCLK_PRE, 2);
	dsi_set_config(DSI_CONFIG_ID_TCLK_TRAIL, 3);

	dsi_set_config(DSI_CONFIG_ID_BTA_HANDSK_TMOUT_VAL, 0x40);
#elif(DSI_TARGET_CLK == 480)
	dsi_set_config(DSI_CONFIG_ID_TLPX, 3);
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_GO, 21);
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_SURE, 0);
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_GET, 20);

	dsi_set_config(DSI_CONFIG_ID_THS_PREPARE, 4);
	dsi_set_config(DSI_CONFIG_ID_THS_ZERO, 6);
	dsi_set_config(DSI_CONFIG_ID_THS_TRAIL, 7);
	dsi_set_config(DSI_CONFIG_ID_THS_EXIT, 6);

	dsi_set_config(DSI_CONFIG_ID_TCLK_PREPARE, 3);
	dsi_set_config(DSI_CONFIG_ID_TCLK_ZERO, 16);
	dsi_set_config(DSI_CONFIG_ID_TCLK_POST, 16);
	dsi_set_config(DSI_CONFIG_ID_TCLK_PRE, 2);
	dsi_set_config(DSI_CONFIG_ID_TCLK_TRAIL, 3);

	dsi_set_config(DSI_CONFIG_ID_BTA_HANDSK_TMOUT_VAL, 0x40);
#elif(DSI_TARGET_CLK == 240)
	dsi_set_config(DSI_CONFIG_ID_TLPX, 3);
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_GO, 21);
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_SURE, 0);
	dsi_set_config(DSI_CONFIG_ID_BTA_TA_GET, 20);

	dsi_set_config(DSI_CONFIG_ID_THS_PREPARE, 4);
	dsi_set_config(DSI_CONFIG_ID_THS_ZERO, 6);
	dsi_set_config(DSI_CONFIG_ID_THS_TRAIL, 7);
	dsi_set_config(DSI_CONFIG_ID_THS_EXIT, 6);

	dsi_set_config(DSI_CONFIG_ID_TCLK_PREPARE, 3);
	dsi_set_config(DSI_CONFIG_ID_TCLK_ZERO, 16);
	dsi_set_config(DSI_CONFIG_ID_TCLK_POST, 16);
	dsi_set_config(DSI_CONFIG_ID_TCLK_PRE, 2);
	dsi_set_config(DSI_CONFIG_ID_TCLK_TRAIL, 3);

	dsi_set_config(DSI_CONFIG_ID_BTA_HANDSK_TMOUT_VAL, 0x40);
//#elif(DSI_TARGET_CLK == 120)
//    dsi_set_config(DSI_CONFIG_ID_TLPX, 0);
//    dsi_set_config(DSI_CONFIG_ID_BTA_TA_GO, 3);

//    dsi_set_config(DSI_CONFIG_ID_THS_PREPARE, 1);
//    dsi_set_config(DSI_CONFIG_ID_THS_ZERO, 2);
//    dsi_set_config(DSI_CONFIG_ID_THS_TRAIL, 1);
//    dsi_set_config(DSI_CONFIG_ID_THS_EXIT, 1);

//    dsi_set_config(DSI_CONFIG_ID_TCLK_PREPARE, 0);
//    dsi_set_config(DSI_CONFIG_ID_TCLK_ZERO, 5);
//    dsi_set_config(DSI_CONFIG_ID_TCLK_POST, 8);
//    dsi_set_config(DSI_CONFIG_ID_TCLK_PRE, 1);
//    dsi_set_config(DSI_CONFIG_ID_TCLK_TRAIL, 1);
//#elif(DSI_TARGET_CLK == 54)
//    dsi_set_config(DSI_CONFIG_ID_TLPX, 1);
//    dsi_set_config(DSI_CONFIG_ID_BTA_TA_GO, 4);

//    dsi_set_config(DSI_CONFIG_ID_THS_PREPARE, 1);
//    dsi_set_config(DSI_CONFIG_ID_THS_ZERO, 0);
//    dsi_set_config(DSI_CONFIG_ID_THS_TRAIL, 1);
//    dsi_set_config(DSI_CONFIG_ID_THS_EXIT, 4);

//    dsi_set_config(DSI_CONFIG_ID_TCLK_PREPARE, 0);
//    dsi_set_config(DSI_CONFIG_ID_TCLK_ZERO, 2);
//    dsi_set_config(DSI_CONFIG_ID_TCLK_POST, 0);
//    dsi_set_config(DSI_CONFIG_ID_TCLK_PRE, 1);
//    dsi_set_config(DSI_CONFIG_ID_TCLK_TRAIL, 1);
#endif
	dsi_set_config(DSI_CONFIG_ID_DATALANE_NO, DSI_DATA_LANE_1);//DSI_DATA_LANE_3
	dsi_set_config(DSI_CONFIG_ID_TE_BTA_INTERVAL, 0x1F);
	dsi_set_config(DSI_CONFIG_ID_CLK_PHASE_OFS, 0x3); //mask check
	//dsi_setConfig(DSI_CONFIG_ID_PHASE_DELAY_ENABLE_OFS, 0x1); //Shaun&KT: must disable clock phase delay

	dsi_set_config(DSI_CONFIG_ID_CLK_LP_CTRL, 0x0);//0x0 check
	dsi_set_config(DSI_CONFIG_ID_SYNC_DLY_CNT, 0xF);

	dsi_set_config(DSI_CONFIG_ID_EOT_PKT_EN, TRUE);//mask check

	// test lane swap 1230
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D0, 1);
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D1, 2);
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D2, 3);
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D3, 0);

	// test lane swap 2301
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D0, 2);
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D1, 3);
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D2, 0);
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D3, 1);

	// test lane swap 3012
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D0, 3);
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D1, 0);
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D2, 1);
	//dsi_set_config(DSI_CONFIG_ID_LANSEL_D3, 2);

}

#if defined __FREERTOS
#if 1
static gamma_t gamma_config =  {
	/*enable = */	1, //0: disable, 1: enable
	/*.gamma_r = */	{0x0,0x8,0x10,0x18,0x20,0x28,0x30,0x38,0x40,0x48,0x50,0x58,0x60,0x68,0x70,0x78,0x80,0x88,0x90,0x98,0xa0,0xa8,0xb0,0xb8,0xc0,0xc8,0xd0,0xd8,0xe0,0xe8,0xf0,0xf8,0xff}, // range 0~255
	/*.gamma_g = */	{0x0,0x8,0x10,0x18,0x20,0x28,0x30,0x38,0x40,0x48,0x50,0x58,0x60,0x68,0x70,0x78,0x80,0x88,0x90,0x98,0xa0,0xa8,0xb0,0xb8,0xc0,0xc8,0xd0,0xd8,0xe0,0xe8,0xf0,0xf8,0xff}, // range 0~255
	/*.gamma_b = */	{0x0,0x8,0x10,0x18,0x20,0x28,0x30,0x38,0x40,0x48,0x50,0x58,0x60,0x68,0x70,0x78,0x80,0x88,0x90,0x98,0xa0,0xa8,0xb0,0xb8,0xc0,0xc8,0xd0,0xd8,0xe0,0xe8,0xf0,0xf8,0xff}, // range 0~255
};
#else
static gamma_t gamma_config =  {
	/*enable = */	1, //0: disable, 1: enable
	/*.gamma_r = */	{0,8,16,24,32,78,113,138,151,154,145,126,96,104,112,120,128,136,144,152,160,168,176,184,192,200,208,216,224,232,240,248,255}, // range 0~255
	/*.gamma_g = */	{0,8,16,24,32,78,113,138,151,154,145,126,96,104,112,120,128,136,144,152,160,168,176,184,192,200,208,216,224,232,240,248,255}, // range 0~255
	/*.gamma_b = */	{0,8,16,24,32,78,113,138,151,154,145,126,96,104,112,120,128,136,144,152,160,168,176,184,192,200,208,216,224,232,240,248,255}, // range 0~255
};
#endif

static cc_t color_control_config = {
	/*.enable = */	0, //0: disable, 1: enable
	/*.sat_ofs = */	0, //range 0~255
	/*.int_ofs = */	0, //range 0~255
	/*.ycon = */	128, //range 0~255
	/*.ccon = */	128, //range 0~255
};
int panel_init(void)
{
	PDISP_OBJ p_disp_obj;
	p_disp_obj = disp_get_display_object(DISP_1);
	p_disp_obj->open();

	// Parsing cfc file if exist
	display_read_cfg(&gamma_config, &color_control_config, DISP_1);
	p_disp_obj->dev_callback = &dispdev_get_lcd1_dev_obj;
	p_disp_obj->close();
    DBG_DUMP("0.Hello, panel: tb093\n");
    return 0;
}

void panel_exit(void)
{
    DBG_DUMP("tb093, Goodbye\r\n");
}

#elif defined __KERNEL__
static int __init panel_init(void)
{
	PDISP_OBJ p_disp_obj;
	p_disp_obj = disp_get_display_object(DISP_1);

	// Parsing cfc file if exist
	if ((strstr(panel_cfg_path, "null")) || (strstr(panel_cfg_path, "NULL"))) {
		DBG_WRN("cfg file no exist \r\n");
		cfg_path[0] = '\0';
	} else {
		if ((panel_cfg_path != NULL) && (strlen(panel_cfg_path) <= 64)) {
			strncpy(cfg_path, panel_cfg_path, 64);
		}

		display_read_cfg(cfg_path, DISP_1);
	}
	p_disp_obj->dev_callback = &dispdev_get_lcd1_dev_obj;
    pr_info("1.Hello, panel: tb093\n");
    return 0;
}

static void __exit panel_exit(void)
{
	PDISP_OBJ p_disp_obj;
	p_disp_obj = disp_get_display_object(DISP_1);

	p_disp_obj->dev_callback = NULL;
    printk(KERN_INFO "Goodbye\n");
}

module_init(panel_init);
module_exit(panel_exit);

MODULE_DESCRIPTION("tb093 Panel");
MODULE_AUTHOR("Novatek Corp.");
MODULE_LICENSE("GPL");
#endif


